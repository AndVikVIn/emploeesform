import {JetView} from 'webix-jet';
import AddEditPopup from './addEditPopup';

class MainPage extends JetView {
    config() {
        this.employeesTable = {
            view: 'datatable',
            localId: 'employeesTable',
            url: '/api/getAll',
            save: '->/api/changeTable',
            select: true,
            scheme: {
                $init(obj) {
                    const item = obj;
                    const format = webix.Date.strToDate('%Y.%m.%d');
                    item.dateOfBirth = format(obj.dateOfBirth);
                    return item;
                }
            },
            columns: [
                {id: 'firstName', header: 'First name', sort: 'string', fillspace: true},
                {id: 'surName', header: 'Surname', sort: 'string', fillspace: true},
                {id: 'dateOfBirth', header: 'Date of birth', format: webix.Date.dateToStr('%d %M %Y'), sort: 'date', fillspace: true},
                {id: 'salary', header: 'Salary', sort: 'int', format: value => `${value} RUB`, fillspace: true}
            ],
            on: {
                onItemDblClick: () => {
                    const item = this.$$('employeesTable').getSelectedItem();
                    if (item) {
                        this.addEditPopup.showPopup(item);
                    }
                },
                onKeyPress: (code) => {
                    if (code === 46 || code === 8) {
                        const item = this.$$('employeesTable').getSelectedItem();
                        if (item) {
                            this.$$('employeesTable').remove(item.id);
                        }
                    }
                }
            }
        };
        this.addButton = {
            view: 'button',
            localId: 'addButton',
            value: 'Add employee',
            click: () => {
                this.addEditPopup.showPopup();
            }
        };
        this.editButton = {
            view: 'button',
            localId: 'editButton',
            value: 'Edit employee',
            click: () => {
                const item = this.$$('employeesTable').getSelectedItem();
                if (item) {
                    this.addEditPopup.showPopup(item);
                }
            }
        };
        this.delButton = {
            view: 'button',
            localId: 'delButton',
            value: 'Delete employee',
            click: () => {
                const item = this.$$('employeesTable').getSelectedItem();
                if (item) {
                    this.$$('employeesTable').remove(item.id);
                }
            }
        };
        return {
            rows: [
                this.employeesTable,
                {cols: [
                    this.addButton,
                    this.editButton,
                    this.delButton
                ]}
            ]
        };
    }
    init() {
        this.addEditPopup = this.ui(AddEditPopup);
        this.on(this.app, 'addemployee', (values) => {
            this.$$('employeesTable').add(values);
        });
        this.on(this.app, 'editemployee', (values) => {
            this.$$('employeesTable').updateItem(values.id, values);
        });
    }
}

export default MainPage;
