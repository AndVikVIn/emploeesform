import {JetView} from 'webix-jet';

class AddEditPopup extends JetView {
    config() {
        return {
            view: 'popup',
            localId: 'addEditPopup',
            position: 'center',
            height: 600,
            width: 700,
            modal: true,
            body: {
                view: 'form',
                localId: 'employeeForm',
                elementsConfig: {
                    labelWidth: 200,
                    labelAlign: 'left'
                },
                elements: [
                    {type: 'header', template: 'Add'},
                    {view: 'text', label: 'First name', attributes: {maxlength: 20}, name: 'firstName', invalidMessage: "Can't be empty"},
                    {view: 'text', label: 'Surname', attributes: {maxlength: 20}, name: 'surName', invalidMessage: "Can't be empty"},
                    {view: 'datepicker', label: 'Date of birth', format: '%d.%m.%Y', name: 'dateOfBirth'},
                    {view: 'text', localId: 'salary', label: 'Salary', name: 'salary', invalidMessage: "Can't be empty"},
                    {
                        cols: [
                            {view: 'button',
                                value: 'Add',
                                localId: 'addButton',
                                click: function updateTable() {
                                    if (this.$scope.$$('employeeForm').validate()) {
                                        const values = this.$scope.$$('employeeForm').getValues();
                                        if (this.getValue() === 'Add') {
                                            this.$scope.app.callEvent('addemployee', [values]);
                                            this.$scope.$$('employeeForm').clear();
                                        }
                                        else {
                                            this.$scope.app.callEvent('editemployee', [values]);
                                            this.$scope.$$('employeeForm').clear();
                                        }
                                        this.$scope.$$('addEditPopup').hide();
                                    }
                                }},
                            {view: 'button',
                                value: 'Cancel',
                                click: () => {
                                    this.$$('employeeForm').clear();
                                    this.$$('addEditPopup').hide();
                                }}
                        ]
                    }
                ],
                rules: {
                    $all: webix.rules.isNotEmpty,
                    salary: (value) => {
                        if (value) {
                            if (!webix.rules.isNumber(value)) {
                                this.$$('salary').define('invalidMessage', 'Incorrect type. PLease use numbers.');
                            }
                            else {
                                return webix.rules.isNotEmpty(value);
                            }
                        }
                        return false;
                    }
                }
            }
        };
    }
    showPopup(item) {
        if (item) {
            this.$$('employeeForm').setValues(item);
            this.$$('addButton').define('value', 'Edit');
        }
        this.getRoot().show();
    }
}

export default AddEditPopup;
