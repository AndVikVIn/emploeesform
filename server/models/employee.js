module.exports = (sequelize, DataTypes) => {
    const employee = sequelize.define('employee', {
        firstName: {
            type: DataTypes.STRING
        },
        surName: {
            type: DataTypes.STRING
        },
        dateOfBirth: {
            type: DataTypes.DATEONLY
        },
        salary: {
            type: DataTypes.INTEGER
        }
    });
    employee.createemployee = async (values) => {
        const result = await employee.create(values);
        return result;
    };
    employee.getAll = async () => {
        const result = await employee.findAll();
        return result;
    };
    employee.getOne = async (id) => {
        const result = await employee.findByPk(id);
        return result;
    };
    employee.updateOne = async (id, values) => {
        const result = await employee.update(values,
            {where: {id}}
        );
        return result;
    };
    employee.deleteOne = async (id) => {
        const result = await employee.destroy({where: {id}});
        return result;
    };
    return employee;
};

