const express = require('express');

const router = express.Router();

router.use(require('./employeeRouter'));

module.exports = router;
