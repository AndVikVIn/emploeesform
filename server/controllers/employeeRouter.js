const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');

const router = express.Router();

const employee = sequelize.import('../models/employee');

const getAll = wrap(async (req, res) => {
    res.send(await employee.getAll());
});

const getOne = wrap(async (req, res) => {
    res.send(await employee.getOne(req.body.id));
});

const changeTable = wrap(async (req, res) => {
    if (req.body.webix_operation === 'delete') {
        const result = await employee.deleteOne(req.body.id);
        if (result === 1) {
            res.json({
                code: 200,
                message: 'Ok',
                response: {msg: 'Recored deleted'}
            });
        }
        else {
            res.json({
                code: 404,
                message: 'Ok',
                response: {msg: 'Not found'}
            });
        }
    }
    else if (req.body.webix_operation === 'update') {
        res.send(await employee.updateOne(req.body.id, req.body));
    }
    else if (req.body.webix_operation === 'insert') {
        res.send(await employee.createemployee(req.body));
    }
});

router.get('/getAll', getAll);
router.get('/getOne', getOne);
router.post('/changeTable', changeTable);


module.exports = router;
