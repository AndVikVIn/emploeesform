const sequelize = require('./dataBase');
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./controllers/mainRouter');

const app = express();

function errorHandler(err, req, res) {
    res.status(500);
    res.render('error', {error: err});
}

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));

app.listen(3000);

app.use('/api', router);

app.use(errorHandler);

process.on('SIGINT', () => {
    sequelize.close();
    process.exit();
});

